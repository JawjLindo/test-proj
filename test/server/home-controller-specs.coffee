#
# Created by Mike Linde <mlinde@lintechsol.com> on 17/05/15.
#

lab = exports.lab = require('lab').script()
require('coffee-script').register()
should = require 'should'
cheerio = require 'cheerio'

lab.experiment 'The Application controller', ->
  server = null

  lab.before (done) ->
    if global.server?
      server = global.server
      done()
    else
      require('../../app/index')()
      .then (testServer) ->
        global.server = server = testServer
        done()
  
  lab.test 'should return the status JSON', (done) ->
    options =
      method: 'GET'
      url: '/api/status'

    server.inject options, (response) ->
      response.statusCode.should.equal 200
      result = response.result
      result.status.should.equal 'running'
      done()

  lab.test 'should return the home page with a valid header and footer', (done) ->
    options =
      method: 'GET'
      url: '/'

    server.inject options, (response) ->
      response.statusCode.should.equal 200
      result = response.result
      $ = cheerio.load result, normalizeWhitespace: true

      # Test header
      headerContainer = $('header').children('div')
      headerContainer.find('a').attr('id').should.equal 'logo'
      headerNavList = headerContainer.find('ul').find('li')
      headerNavList[0].children[0].children[0].data.should.equal 'Home'
      headerNavList[1].children[0].children[0].data.should.equal 'Help'

      # Test body
      $('title')[0].children[0].data.should.equal 'Generated Application'
      bodyContainer = $('#body-container')
      should.exist bodyContainer.find('.center')[0]

      # Test footer
      small = $('footer.footer').find('small')[0]
      small.children[0].data.trim().should.equal 'Generated code by'
      small.children[1].children[0].data.trim().should.equal 'Mike Linde'
      footerNavList = $('footer.footer').find('nav').find('ul').find('li')
      footerNavList[0].children[0].children[0].data.should.equal 'About'
      footerNavList[1].children[0].children[0].data.should.equal 'Contact'

      done()