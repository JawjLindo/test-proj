#
# Created by Mike Linde <mlinde@lintechsol.com> on 17/05/15.
#

Joi = require 'joi'
resource = require 'hapi-resourceful-routes'

loadHomeRoutes = (routes) ->
  Controller = require '../app/controllers/application-controller'

  routes.push
    method: 'GET'
    path: '/'
    config:
      id: 'home.get'
      tags: ['pages']
      handler: Controller.home
      description: 'Home page of the application'

  routes.push
    method: 'GET'
    path: '/api/status'
    config:
      id: 'api.status'
      handler: Controller.statusApiHandler
      tags: ['api']
      description: 'API for the current status of the server'
      response:
        schema:
          Joi.object().keys
            status: Joi.string().required().valid('running', 'stopped')

  routes.push
    method: 'GET'
    path: '/help'
    config:
      id: 'help.get'
      handler: Controller.help
      tags: ['pages']
      description: 'Help page for the application'

  routes.push
    method: 'GET'
    path: '/about'
    config:
      id: 'about.get'
      handler: Controller.about
      tags: ['pages']
      description: 'About the sample'

  routes.push
    method: 'GET'
    path: '/contact'
    config:
      id: 'contact.get'
      handler: Controller.contact
      tags: ['pages']
      description: 'Contact Information'


module.exports = exports = (server) ->
  routes = []

  loadHomeRoutes routes

  server.route routes