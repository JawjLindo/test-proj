#
# Created by Mike Linde <mlinde@lintechsol.com> on 17/05/15.
#

gulp = require 'gulp'
gutil = require 'gulp-util'
concat = require 'gulp-concat'
sourcemaps = require 'gulp-sourcemaps'
del = require 'del'
runSequence = require 'run-sequence'
sass = require 'gulp-sass'
coffee = require 'gulp-coffee'
lab = null
env = require 'gulp-env'
watch = null
bower = require 'gulp-bower'
nodemon = null

if process.env.NODE_ENV isnt 'production'
  nodemon = require 'gulp-nodemon'
  lab = require 'gulp-lab'
  watch = require 'gulp-watch'

gulp.task 'clean-build', (cb) ->
  del [
    './build'
  ], cb

gulp.task 'compile-sass', ->
  gulp.src './app/assets/sass/**/*.scss'
  .pipe sourcemaps.init()
  .pipe sass().on 'error', gutil.log
  .pipe gulp.dest './build/css'
  .pipe concat 'styles.css'
  .pipe sourcemaps.write '.'
  .pipe gulp.dest './public/css'

gulp.task 'compile-client-scripts', ->
  gulp.src [
    './app/assets/js/**/*.coffee'
  ]
  .pipe sourcemaps.init()
  .pipe coffee().on 'error', gutil.log
  .pipe gulp.dest './build/js'
  .pipe concat 'app-scripts.js'
  .pipe sourcemaps.write '.'
  .pipe gulp.dest './public/js'

gulp.task 'compile-assets', (cb) ->
  runSequence 'clean-build', [
    'compile-sass'
    'compile-client-scripts'
  ], cb

gulp.task 'compile-server-specs', ->
  gulp.src './test/**/*.coffee'
  .pipe sourcemaps.init()
  .pipe coffee( bare: true ).on 'error', gutil.log
  .pipe sourcemaps.write '.'
  .pipe gulp.dest './test/'

gulp.task 'set-test-env', ->
  env
    vars:
      NODE_ENV: 'test'

gulp.task 'run-server-specs', ['compile-server-specs', 'set-test-env'], ->
  gulp.src './test/server/**/*.js'
  .pipe lab '-v -l -C'

gulp.task 'develop', ['compile-assets', 'run-server-specs'], ->
  nodemon({
    script: 'app/index.coffee'
    ext: 'coffee json scss handlebars'
    restartable: 'rs'
    verbose: false
    env:
      NODE_ENV: 'development'
    watch: [
      'app/**/*'
      'config/**/*'
      'test/**/*.coffee'
      'the-gulpfile.coffee'
      'package.json'
    ]
  }).on 'restart', ->
    runSequence 'compile-assets', 'run-server-specs'

gulp.task 'clean', ['clean-build'], (cb) ->
  del [
    'logs'
    'test/server/**/*.js'
    'test/server/**/*.js.map'
    'npm-debug.log'
    'public/vendors'
    'public/css'
    'public/js'
  ], cb

gulp.task 'test', [
  'run-server-specs'
]

gulp.task 'bower', ->
  bower()
  .pipe gulp.dest 'public/vendors/'

gulp.task 'postinstall', [
  'build'
]

gulp.task 'build', (cb) ->
  runSequence 'clean',
    'compile-assets',
    'bower',
    'clean-build',
    cb