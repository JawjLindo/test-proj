FROM node:0.12.4

COPY . /src
RUN cd /src && npm install --unsafe-perm

EXPOSE 3000

WORKDIR /src

CMD ["npm", "start"]
