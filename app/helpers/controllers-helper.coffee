#
# Created by Mike Linde <mlinde@lintechsol.com> on 19/05/15.
#

getFullPageTitle = (name) ->
  baseTitle = 'Generated Application'
  return baseTitle if not name?
  "#{name} | #{baseTitle}"
exports.getFullPageTitle = getFullPageTitle