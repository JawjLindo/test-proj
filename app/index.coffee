#
# Created by Mike Linde <mlinde@lintechsol.com> on 17/05/15.
#

Hapi = require 'hapi'
Promise = require 'bluebird'
path = require 'path'
config = require 'config'
mkdirp = Promise.promisify require 'mkdirp'

process.env.NODE_ENV = config.get 'NODE_ENV' if not process.env.NODE_ENV

server = new Hapi.Server

loadGoodPlugin = ->
  options =
    opsInterval: 1000
    reporters: [
      {
        reporter: require 'good-console'
        events: { log: '*' }
      },
      {
        reporter: require 'good-file'
        events: { log: '*', response: '*' }
        config: config.get 'logs.info.path'
      },
      {
        reporter: require 'good-file'
        events: { error: '*' }
        config: config.get 'logs.error.path'
      }
    ]

  return new Promise (resolve, reject) ->
    mkdirp config.get('logs.base-path')
    .then ->
      server.register {
        register: require 'good'
        options: options
      }, (err) ->
        if err?
          server.log ['error'], message: 'Could not load \'good\' plugin'
          return reject err

        server.log ['info'], message: 'Loaded \'good\' plugin'
        resolve()
    .catch (err) ->
      reject err

loadStaticContentPlugin = ->
  options =
    path: config.get 'assets.public-root'

  return new Promise (resolve, reject) ->
    server.register {
      register: require 'hapi-static-content'
      options: options
    }, (err) ->
      if err?
        server.log ['error'], message: 'Could not load \'hapi-static-content\' plugin'
        return reject err

      server.log ['info'], message: 'Loaded \'hapi-static-content\' plugin'
      resolve()

loadLoutPlugin = ->
  options = {}

  return new Promise (resolve, reject) ->
    server.register {
      register: require 'lout'
      options: options
    }, (err) ->
      if err?
        server.log ['error'], message: 'Could not load \'lout\' plugin'
        return reject err

      server.log ['info'], message: 'Loaded \'lout\' plugin'
      resolve()

loadRoutes = ->
  return new Promise (resolve) ->
    require('../config/routes') server
    resolve()

configureViewRendering = ->
  return new Promise (resolve) ->
    viewsPath = path.join __dirname, 'views'
    server.views
      engines: { handlebars: require('handlebars').create() }
      path: viewsPath
      layoutPath: path.join viewsPath, 'layout'
      layout: true
      partialsPath: path.join viewsPath, 'partials'
    resolve()

startServer = ->
  return new Promise (resolve, reject) ->
    server.start (err) ->
      if err?
        server.log ['error'], message: 'Server could not be started'
        return reject err

      server.log ['info'], message: "Server started at #{server.info.uri}"
      resolve()

if not module.parent?
  connectionOptions =
    host: process.env.IP || config.get 'server.host'
    port: process.env.PORT || config.get 'server.port'
  server.connection connectionOptions

  loadGoodPlugin()
  .then -> loadStaticContentPlugin()
  .then -> loadLoutPlugin()
  .then -> configureViewRendering()
  .then -> loadRoutes()
  .then -> startServer()
  .catch (err) ->
    throw err
else
  module.exports = ->
    return new Promise (resolve, reject) ->
      server.connection()
      configureViewRendering()
      .then -> loadRoutes()
      .then -> resolve server
      .catch (err) ->
        reject err